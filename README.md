# Reactor module for Corona SDK

## How to use it

**Load and initialize Reactor in `main.lua` with Your Application ID:**
```lua
reactor = require('reactor')
reactor:init('1570e1e-5744-21e3-b1c8-78ee943e8r45')
```

**Collect data:**
```lua
reactor:collect('user123', {
	first_name_s = 'John',
	last_name_s = 'Stone',
	age_i = 33
})
```

**Send events:**
```lua
reactor:event('user123', 'start_game', {
	level_i = 1
})
```

**Send bulk data:**
```lua
reactor:bulk('user123', {
	{type = 'event', data = {type = 'start_game', level_i = 1}},
	{type = 'collect', data = {first_name_s = 'John', last_name_s = 'Stone'}},
	{type = 'event', data = {type = 'stop_game', score_i = 123}},
})
```

Find more details about parameters on [www.reactor.am/docs](http://www.reactor.am/docs) page.

## Issues
* cache data when network is not available